defmodule Pulldash.IssueController do
  use Pulldash.Web, :controller
  alias Pulldash.Issue
  alias Pulldash.Repo

  plug :action

  def index(conn, _params) do
    issues = Repo.all(Issue)
    render conn, issues: issues
  end

  def show(conn, %{"id" => id}) do
    render conn, issue: Repo.get(Issue, id)
  end
end
