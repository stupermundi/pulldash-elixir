defmodule Pulldash.RepoController do
  use Pulldash.Web, :controller
  alias Pulldash.Repo
  alias Pulldash.GitRepo

  plug :action

  def index(conn, _params) do
    repos = Repo.all(GitRepo)
    render conn, repos: repos
  end

  def show(conn, %{"id" => id}) do
    render conn, repo: Repo.get(GitRepo, id)
  end

  def issues(conn, %{"user" => user, "name" => name}) do
    query = from r in GitRepo,
            where: r.user == ^user and r.name == ^name

    repo = Repo.one!(query)

    repo = repo |> Repo.preload(:issues)

    render conn, issues: repo
  end
end
