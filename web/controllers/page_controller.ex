defmodule Pulldash.PageController do
  use Pulldash.Web, :controller

  plug :action

  def index(conn, _params) do
    render conn, "index.html"
  end
end
