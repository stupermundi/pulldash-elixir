defmodule Pulldash.RepoView do
  use Pulldash.Web, :view

  def render("index.json", %{repos: repos}) do
    repos
  end

  def render("show.json", %{repo: repo}) do
    repo
  end

  def render("issues.json", %{issues: issues}) do
    issues
  end  
end
