defmodule Pulldash.IssueView do
  require Logger
  use Pulldash.Web, :view

  def render("index.json", %{issues: issues}) do
    issues
  end

  def render("show.json", %{issue: issue}) do
    issue
  end
end
