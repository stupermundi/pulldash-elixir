defmodule Pulldash.Router do
  use Pulldash.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :assign_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Pulldash do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/hello", HelloController, :index
    get "/hello/:messenger", HelloController, :show
  end

  scope "/auth", Pulldash do
    pipe_through :browser

    get "/", AuthController, :index
    get "/callback", AuthController, :callback
   end

  # Other scopes may use custom stacks.
  scope "/api", Pulldash do
    pipe_through :api

    resources "issues", IssueController, only: [:index, :show]
    resources "repos", RepoController, only: [:index, :show]
    get "/repos/:user/:name/", RepoController, :issues
  end

  # Fetch the current user from the session and add it to `conn.assigns`. This
  # will allow you to have access to the current user in your views with
  # `@current_user`.
  defp assign_current_user(conn, _) do
    assign(conn, :current_user, get_session(conn, :current_user))
  end
end
