defmodule Pulldash.GitRepo do
  use Ecto.Model


  schema "git_repos" do
    field :user
    field :name
    field :url
    field :site

    timestamps

    has_many :issues, Pulldash.Issue, foreign_key: :repo_id
  end
end

