defmodule Pulldash.Issue do
  use Ecto.Model

  
  schema "issues" do
    field :issue_id,  :integer
    field :name
    belongs_to :repo, Pulldash.GitRepo
    field :open,      :boolean
    field :stats,     {:array, :string}

    timestamps
  end
end
