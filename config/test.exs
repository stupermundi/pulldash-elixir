use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :pulldash, Pulldash.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :pulldash, Pulldash.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "pulldash",
  password: "pulldash",
  database: "pulldash_test",
  size: 1 # Use a single connection for transactional tests
