defmodule Pulldash.IssueControllerTest do
  use Pulldash.Case, async: false

  test "/repos" do
    repos =
     %GitRepo{name: "testing", user: "freshvolk", url: "https://github.com/freshvolk/testing", site: "github"}
      |> Repo.insert
      |> List.wrap
      |> Poison.encode!

    response = conn(:get, "/api/repos") |> send_request

    assert response.resp_body == repos
    assert response.status == 200
  end

  test "/issues returns a list of issues" do
    repos =
     %GitRepo{name: "damn", user: "freshvolk", url: "https://github.com/freshvolk/damn", site: "github"}
      |> Repo.insert
    issue_as_json =
     %Issue{issue_id: 116, name: "Dependency bundling", repo_id: repos.id, open: false, stats: ["cr", "qa"]}
      |> Repo.insert
      |> List.wrap
      |> Poison.encode!

    response = conn(:get, "/api/issues") |> send_request

    assert response.status == 200
    assert response.resp_body == issue_as_json
  end

  test "/issues/:id returns an issue" do
    repos =
     %GitRepo{name: "dur", user: "freshvolk", url: "https://github.com/freshvolk/dur", site: "github"}
      |> Repo.insert
    issue =
     %Issue{issue_id: 116, name: "Dependency", repo_id: repos.id, open: false, stats: ["cr", "qa"]}
      |> Repo.insert

    issue_as_json = Poison.encode!(issue)

    issue_id =  "/api/issues/" <> inspect issue.id

    response = conn(:get, issue_id) |> send_request

    assert response.status == 200
    assert response.resp_body == issue_as_json
  end

end
