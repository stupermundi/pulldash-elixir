defmodule Pulldash.Case do
  use ExUnit.CaseTemplate
  alias Ecto.Adapters.SQL
  alias Pulldash.Repo


  setup do
    SQL.begin_test_transaction(Repo)

    on_exit fn ->
      SQL.rollback_test_transaction(Repo)
    end
  end

  using do
    quote do
      alias Pulldash.Repo
      alias Pulldash.Issue
      alias Pulldash.GitRepo
      use Plug.Test

      def send_request(conn) do
        conn
        |> put_private(:plug_skip_csrf_protection, true)
        |> Pulldash.Endpoint.call([])
      end
    end
  end
end


ExUnit.start

# Create the database, run migrations, and start the test transaction.
Mix.Task.run "ecto.create", ["--quiet"]
Mix.Task.run "ecto.migrate", ["--quiet"]
Ecto.Adapters.SQL.begin_test_transaction(Pulldash.Repo)
