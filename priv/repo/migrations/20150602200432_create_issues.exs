defmodule Pulldash.Repo.Migrations.CreateIssues do
  use Ecto.Migration

  def up do
    create table(:issues) do
      add :issue_id,  :integer
      add :name,      :string
      add :repo_id,   references(:git_repos)
      add :open,      :bool
      add :stats,     {:array, :string}

      timestamps
    end
  end


  def down do
    drop table(:issues)
  end
end

