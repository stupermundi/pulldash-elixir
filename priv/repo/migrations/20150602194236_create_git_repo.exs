defmodule Pulldash.Repo.Migrations.CreateGitRepo do
  use Ecto.Migration

  def up do
    create table(:git_repos) do
      add :user,    :string
      add :name,    :string
      add :url,     :string
      add :site,    :string

      timestamps
    end
  end

  def down do
    drop table(:git_repos)
  end
end
